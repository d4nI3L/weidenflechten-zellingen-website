export const CardContentLanding = [
    {
        category: "Weidenkörbe",
        dir: "baskets/",
        filename: "basket1.jpg",
        heading: "Weidenkorb",
        description: "Individuell, ob als Feuerholzkorb, für Lebensmittel, blumen und vieles mehr. Nachhaltig, praktisch und zeitlos schön.",
        to: "/#weidenkoerbe"
    },
    {
        category: "Weidentaschen",
        dir: "bags/",
        filename: "bag1.jpg",
        heading: "Weidentasche",
        description: "Korbtasche mit Charme und Chick. Henkel aus echtem Naturleder in verschiedenen Farben. Einkaufen mit gutem Gewissen, nachhaltig und verantwortungsbewußt.",
        to: "/#weidentaschen"
    },
    {
        category: "Dekoration",
        dir: "deco/",
        filename: "deco1.jpg",
        heading: "Weidenvase",
        description: "Vielerlei Ideen mit Weiden aller Art, um Haus und Garten zu einem echten Blickfang zu gestalten.",
        to: "/#dekoration"
    }
]

export const ContentCraftsBaskets = [
    {
        category: "Weidenkörbe",
        dir: "baskets/",
        filename: "basket6.jpg",
        heading: "Katzenkorb aus Weide",
    },
    {
        category: "Weidenkörbe",
        dir: "baskets/",
        filename: "basket2.jpg",
        heading: "Weidenkorb dekoriert mit Zwetschgen",
    },
    {
        category: "Weidenkörbe",
        dir: "baskets/",
        filename: "basket3.jpg",
        heading: "Rustikaler Weidenkorb",
    },
    {
        category: "Weidenkörbe",
        dir: "baskets/",
        filename: "basket4.jpg",
        heading: "Weidenkorb mit Henkel",
    },
    {
        category: "Weidenkörbe",
        dir: "baskets/",
        filename: "basket5.jpg",
        heading: "Weidenkorb mit Griffen",
    },
    {
        category: "Weidenkörbe",
        dir: "baskets/",
        filename: "basket8.jpg",
        heading: "Weidenkorb mit Griffen",
    },
    {
        category: "Weidenkörbe",
        dir: "baskets/",
        filename: "basket7.jpg",
        heading: "Katzenkörbe aus Weide",
    },
    {
        category: "Weidenkörbe",
        dir: "baskets/",
        filename: "basket1.jpg",
        heading: "Weidenkorb mit geflochtenen Griffen",
    }
]

export const ContentCraftsBags = [
    {
        category: "Weidentaschen",
        dir: "bags/",
        filename: "bag1.jpg",
        heading: "Stylische braune Weidentasche",
    },
    {
        category: "Weidentaschen",
        dir: "bags/",
        filename: "bag2.jpg",
        heading: "Stylische braune Weidentasche",
    },
    {
        ategory: "Weidentaschen",
        dir: "bags/",
        filename: "bag6.jpg",
        heading: "Weidentasche mit rotem Leder",
    },
    {
        category: "Weidentaschen",
        dir: "bags/",
        filename: "bag4.jpg",
        heading: "Weidentasche mit braunem Leder",
    },
    {
        category: "Weidentaschen",
        dir: "bags/",
        filename: "bag5.jpg",
        heading: "Weidentasche mit braunem Leder",
    },
    {
        category: "Weidentschen",
        dir: "bags/",
        filename: "bag3.jpg",
        heading: "Weidentasche mit braunem Leder",
    },
    {
        category: "Weidentschen",
        dir: "bags/",
        filename: "bag7.jpg",
        heading: "Modische rote Weidentasche",
    },
    {
        category: "Weidentschen",
        dir: "bags/",
        filename: "bag8.jpg",
        heading: "Zeitlos moderne Weidentasche",
    }
]


export const ContentCraftsDeco = [
    {
        category: "Dekoration",
        dir: "deco/",
        filename: "deco1.jpg",
        heading: "Weidenvase",
    },
    {
        category: "Dekoration",
        dir: "deco/",
        filename: "deco5.jpg",
        heading: "Weidenrassel",
    },
    {
        ategory: "Dekoration",
        dir: "deco/",
        filename: "deco3.jpg",
        heading: "Weiden-Gartenschaukel",
    },
    {
        category: "Dekoration",
        dir: "deco/",
        filename: "deco6.jpg",
        heading: "Obstschale aus Weide",
    },
    {
        category: "Dekoration",
        dir: "deco/",
        filename: "deco2.jpg",
        heading: "Gartendeko",
    },
    {
        category: "Dekoration",
        dir: "deco/",
        filename: "deco4.jpg",
        heading: "Weiden-Rankhilfe",
    }
]