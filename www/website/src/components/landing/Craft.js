import React, {Component} from 'react';
import {ContentCraftsBaskets, ContentCraftsBags, ContentCraftsDeco} from "../../constants/Contents"
import GalleryImageHolder from "./GalleryImageHolder";
import {Element} from 'react-scroll';

class Craft extends Component {
    render() {
        return <div>
            <Element id="weidenkoerbe" name="weidenkoerbe" className="mx-6 my-5 text-container">
                <div className="text-center content-text-dark">
                    <h2>Unsere Weidenkörbe</h2>
                    <div className="spacer-small"/>
                    <h3>
                        Nachhaltig | Traditionell | Zeitlos | Praktisch
                    </h3>
                </div>
                <div className="spacer-small"/>
                <div className="row">
                    {ContentCraftsBaskets.map(it => {
                        return <GalleryImageHolder key={it.heading + it.filename} image={it}/>
                    })}
                </div>
            </Element>
            <Element id="weidentaschen" name="weidentaschen" className="mx-6 my-5 text-container">
                <div className="text-center content-text-dark">
                    <h2>Unsere Weidentaschen</h2>
                    <div className="spacer-small"/>
                    <h3>
                        Modisch | Stilvoll | Zeitlos | Bewusst
                    </h3>
                </div>
                <div className="spacer-small"/>
                <div className="row">
                    {ContentCraftsBags.map(it => {
                        return <GalleryImageHolder key={it.heading + it.filename} image={it}/>
                    })}
                </div>
            </Element>
            <Element id="dekoration" name="dekoration" className="mx-6 my-5 text-container">
                <div className="text-center content-text-dark">
                    <h2>Unsere Dekoration</h2>
                    <div className="spacer-small"/>
                    <h3>
                        Dezent | Nachhaltig | Bewusst | Traditionell
                    </h3>
                </div>
                <div className="spacer-small"/>
                <div className="row">
                    {ContentCraftsDeco.map(it => {
                        return <GalleryImageHolder key={it.heading + it.filename} image={it}/>
                    })}
                </div>
            </Element>
        </div>
    }
}

export default Craft;