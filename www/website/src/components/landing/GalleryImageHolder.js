import React, {Component} from "react";
import {MDBModal, MDBModalBody, MDBModalHeader} from "mdbreact";
import {trackWindowScroll, LazyLoadImage} from "react-lazy-load-image-component";

class Placeholder extends Component {

    render() {
        return <div className="w-100 weiden-gradient placeholder"/>
    }
}

class GalleryImageHolder extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            showTip: false
        }
    }

    toggleModal = () => {
        this.setState({
            modal: !this.state.modal
        });
    }

    handleEnter = () => this.setState({ showTip: true});
    handleLeave = () => this.setState({showTip: false});

    renderTooltip() {
        // check once if device is mobile or not otherwise the user can use the mouse to hover
          return window.screen.width >= 1280 ?
               <div className="tooltip">
                <p>Klicken, um Bild zu vergößern</p>
            </div> : null;
    }

    render() {

        const {image, scrollPosition} = this.props;

        return <>
            <div className="col-sm-12  col-xl-3 col-lg-6 col-md-6 col-12 mt-3 mb-3">
                { this.state.showTip === true ? this.renderTooltip() : null}
                <div className="card nice-zoom h-100 decent-bg" onClick={this.toggleModal}>
                    <div className="card-container max-card text-center" onMouseOver={this.handleEnter} onMouseLeave={this.handleLeave}>
                        <LazyLoadImage
                            placeholder={
                                <Placeholder/>
                            }
                            className="gallery-img"
                            src={"https://www.weidenflechten-zellingen.de/img/" + image.dir + image.filename}
                            alt={image.heading}
                            effect="blur"
                            scrollPosition={scrollPosition}
                        />
                        {console.log(window.innerWidth)}
                    </div>
                    <div className="card-footer text-center">
                        <h6 className="card-title">{image.heading}</h6>
                    </div>
                </div>
            </div>
            <MDBModal isOpen={this.state.modal} toggle={this.toggleModal} size="lg">
                <MDBModalHeader toggle={this.toggleModal}>{image.heading}</MDBModalHeader>
                <MDBModalBody>
                    {/* here i can use the image without lazy cause its already loaded in cache*/}
                    <img className="img-fluid"
                         src={"https://www.weidenflechten-zellingen.de/img/" + image.dir + image.filename}
                         alt={image.heading}
                    />
                </MDBModalBody>
            </MDBModal>
        </>
    }
}

export default trackWindowScroll(GalleryImageHolder);