import React, {Component} from 'react';
import {Element} from 'react-scroll';
import {LazyLoadImage} from "react-lazy-load-image-component";

class AboutUs extends Component {

    render() {
        return (
            <Element id="about" name="about">
                <div className="card card-cascade wider reverse text-container">
                    <div className="card-body card-body-cascade">
                        <div className="text-center my-3">
                            <h2 className="card-title content-text-dark"><strong>Über uns und unser Handwerk</strong>
                            </h2>
                            <br/>
                        </div>
                        <div className="tex-center row">
                            <div className="my-3 col-xl-4 col-lg-12 col-md-12">
                                <LazyLoadImage src={"https://www.weidenflechten-zellingen.de/img/us.jpg"} alt="Weinberge in Zellingen und wir" className="img-fluid" effect="blur"/>
                            </div>
                            <div className="my-3 col-xl-7 col-lg-12 col-md-12">
                                <div className="my-card-text continuous-text">
                                    <p>
                                        Die kreative Seite im Leben zum Ausdruck zu bringen, war uns schon immer
                                        wichtig. Sie in Verbindung zu setzen mit unserer Einstellung zu einem
                                        Leben in Achtung und Respekt zur Natur, brachte uns zu unserem Hobby
                                        Weidenflechten. Hier hatten wir die Möglichkeit etwas aus der Natur -
                                        für die Natur zu machen, denn Produkte aus Weiden herzustellen bedeutet
                                        praktizierter Natur- und Umweltschutz.

                                    </p>
                                    <br/>
                                    <p>
                                        Das Flechten haben wir durch Kurse und mit viel Übung und Geduld in den
                                        Jahren stets mit wachsender Leidenschaft weiter entwickelt. Es ist für
                                        uns ein herrlicher Ausgleich zum Berufsalltag geworden.
                                        Und vielleicht können wir durch unser liebgewordenes Hobby
                                        Weidenflechten auch einen Beitrag leisten, die alte Handwerkstradition
                                        des Korbflechtens in Ehren zu halten.
                                    </p>
                                </div>
                                <h3 className="card-title content-text-dark">Faszination Weidenflechten</h3>
                                <div className="my-card-text continuous-text">
                                    <p>
                                        Weiden sind ein faszinierender Werkstoff, den unsere Natur bietet. Es
                                        gibt sie in vielen Arten und Farben. Und das Beste daran ist die
                                        Nachhaltigkeit, denn die Weiden sind ein nachwachsender Rohstoff, der im
                                        Jahreskreislauf uns immer wieder aufs Neue zur Verfügung gestellt wird.
                                        Faszinierend ist auch, welche Tragkraft und Robustheit die Weiden
                                        entwickeln, wenn sie in alter Handwerkskunst zu Körben und vielen
                                        anderen schönen Dingen verflochten werden. Weidengeflechte sind von
                                        zeitloser Schönheit und stehen in perfekter Kombination zu blumen und
                                        anderen Pflanzen.
                                    </p>
                                    <br/>
                                    <p>
                                        Zum Einkaufen verwendete Körbe und Taschen aus Weiden haben ihren
                                        eigenen Charme und sind keine Massenware, sondern individueller Ausdruck
                                        von Natürlichkeit und Nachhaltigkeit. Mit dem Gebrauch von Weidenkörben
                                        bzw. -taschen und dem Verzicht auf Plastiktüten kann ein
                                        verantwortungsbewusster Beitrag zum Schutz unserer Umwelt geleistet werden.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Element>);
    }
}

export default AboutUs;