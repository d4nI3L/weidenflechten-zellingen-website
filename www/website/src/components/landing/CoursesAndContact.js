import React, {Component, lazy, Suspense} from 'react';
import {Element} from 'react-scroll';
import {LazyLoadImage} from "react-lazy-load-image-component";

const Map = lazy(() => import('../map/Map'));

class CoursesAndContact extends Component {

    render() {
        return (
            <Element id="kurse" name="kurse">
                <div className="card card-cascade wider reverse text-container">
                    <div className="card-body card-body-cascade">
                        <div className="text-center my-3">
                            <h2 className="card-title content-text-dark"><strong>Unsere Türe steht immer für Euch
                                offen!</strong></h2>
                            <br/>
                        </div>
                        <div className="tex-center row">
                            <div className="my-3 col-lg-6 col-md-12">
                                <h3 className="font-weight-bold content-text-dark">Kontakt:</h3>
                                <div className="emphasised-text">
                                    <br/>
                                    <p className="my-card-text">Sabine und Volker</p>
                                    <p className="my-card-text">Vorstadt 27</p>
                                    <p className="my-card-text">97225 Zellingen</p>
                                    <br/>
                                    <p className="my-card-text">Telefon: 09364 4671</p>
                                    <p className="my-card-text">E-Mail:
                                        <a
                                            className="avocado"
                                            href="mailto:info@weidenflechten-zellingen.de?subject=Anfrage"> info@weidenflechten-zellingen.de
                                        </a>
                                    </p>
                                    <br/>
                                </div>
                                <div className="my-card-text continuous-text">
                                    Wir laden Euch ein mit unserer Internetseite ein wenig in unsere Weidenwelt
                                    einzutauchen.
                                    Für mehr Impressionen schaut doch einfach mal
                                    auf <a className="avocado" href="https://www.pinterest.de/volkerwingenfel/">Pinterest</a> vorbei
                                    oder folgt uns auf <a className="avocado" href="https://www.instagram.com/weidenflechten.zellingen/">Instagram</a>
                                    . Wir besuchen auch hin und wieder Märkte und bieten nach Absprache auch Flechtkurse an.
                                    Gerne geben wir Euch Hilfestellung, wenn Ihr einmal selbst das Flechten mit
                                    Naturmaterial ausprobieren möchtet.
                                </div>
                            </div>
                            <div className="col-xl-2"/>
                            <div className="my-5 col-xl-4 col-md-12">
                                <LazyLoadImage src={"https://www.weidenflechten-zellingen.de/img/Kontakt.jpg"}
                                     alt="Unsere Tür steht dir immer offen!" className="img-fluid" effect="blur"/>
                            </div>
                            <div className="my-3 col-lg-12">
                                <h2 className="font-weight-bold content-text-dark">Kurse:</h2>
                                <div className="my-card-text continuous-text">
                                    Falls Ihr Interesse habt auch einmal selbst
                                    etwas Schönes aus Weiden oder für euren Garten oder euer Zuhause zu flechten, dann
                                    meldet euch einfach bei uns.
                                    Nähere Details zum Kurs, wie Termin, oder was wir flechten wollen, die Kosten, usw.
                                    können wir gerne gemeinsam absprechen.
                                </div>
                            </div>
                            <div className="col-xl-2"/>
                            <div className="my-3 col-lg-12">
                                <h3 className="font-weight-bold content-text-dark">
                                    Hier findet Ihr uns:
                                </h3>
                                <Suspense fallback={<div className="row align-content-center my-map-loader">
                                    <div className="col-12 my-auto text-center">
                                        <div className="spinner-border my-spinner-loader avocado"/>
                                    </div>
                                </div>}>
                                    <Map/>
                                </Suspense>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="spacer"/>
            </Element>);
    }

}

export default CoursesAndContact;