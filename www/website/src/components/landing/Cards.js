import React, {Component} from 'react';
import {NavHashLink as NavLink} from 'react-router-hash-link';
import {LazyLoadImage} from "react-lazy-load-image-component";


class Cards extends Component {

    render() {
        const {content} = this.props;

        return <section id="unser-handwerk" className="mx-6 my-5 text-container">
            <p className="huge-text text-center content-text-dark my-5">
                Nachhaltig | Traditionell | Gut
            </p>
            <div className="row">
                {content.map(it => {
                    return <div key={it.filename} className="col-sm-12  col-xl-4 my-3">
                        <div className="card nice-zoom h-100">
                            <div className="card-container max-card">
                                <LazyLoadImage
                                    alt={it.heading}
                                    src={"https://www.weidenflechten-zellingen.de/img/" + it.dir + it.filename} // use normal <img> attributes as props
                                    className="img-fluid"
                                    effect="blur"/>
                            </div>
                            <div className="card-body">
                                <h4 className="card-title">{it.heading}</h4>
                                <p className="continuous-text fog">
                                    {it.description}
                                </p>
                            </div>
                            <div className="card-footer text-center mt-auto mb-auto">
                                <NavLink to={it.to} className="btn avocado-bg content-text-dark mycard-btn">
                                    {it.category}
                                </NavLink>
                            </div>
                        </div>
                    </div>
                })}
            </div>
        </section>
    }
}

export default Cards;