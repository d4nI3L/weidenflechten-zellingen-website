import React, {Component} from 'react';
import 'bootstrap-css-only/css/bootstrap.min.css';
import '../../index.css';
import {MDBMask, MDBView} from "mdbreact";
import {NavHashLink as NavLink} from 'react-router-hash-link';


class FullSizeBanner extends Component {

    render() {

        const {imgName} = this.props;

        return <MDBView src={"https://www.weidenflechten-zellingen.de/img/" + imgName}>
            <MDBMask overlay="black-light" className="flex-center flex-column text-white text-center">
                <div className="nice-white-text">
                    <h1 className="nice-white-text decent-bold">Weidenflechten Zellingen</h1>
                    <h2 className="nice-white-text decent-bold">Tradition & Nachhaltigkeit </h2>
                    <h3 className="nice-white-text decent-bold">Mit dieser Philosophie und Leidenschaft flechten wir unsere Weiden</h3>
                    <br/>
                    <p className="nice-white-text nav-item">Zeitlos | Praktisch | Naturbelassen | Kulturgut</p>
                </div>
                <div className="mt-2">
                    <NavLink to="/#about" className="btn avocado-bg content-text-dark mycard-btn">
                        Erfahre mehr über uns
                    </NavLink>
                </div>
            </MDBMask>
        </MDBView>
    }
}

export default FullSizeBanner