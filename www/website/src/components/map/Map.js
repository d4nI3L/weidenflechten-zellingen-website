import React, {Component} from 'react';
import {API_TOKEN} from "../../constants/Token";
import MapGL, {NavigationControl, FullscreenControl, ScaleControl, Popup} from 'react-map-gl';
import MapMarker from "./MapMarker";



const fullscreenControlStyle = {
    position: 'absolute',
    top: 0,
    left: 0,
    padding: '10px'
};

const navStyle = {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: '10px'
};

const scaleControlStyle = {
    position: 'absolute',
    bottom: 36,
    left: 0,
    padding: '10px'
};

const ADDRESS ={longitude: 9.814426, latitude: 49.900043};

class Map extends Component {

    constructor(props) {
        super(props);
        this.state = {
            viewport: {
                longitude: 9.814426,
                latitude: 49.900043,
                zoom: 12,
                bearing: 0,
                pitch: 0
            },
            popupInfo: null
        };

        this._onClickMarker = this._onClickMarker.bind(this);
    }

    onChangeViewport = viewport => this.setState({viewport});

    _onClickMarker = address => {
        this.setState({popupInfo: address});
    };

    __renderPopup() {
        const {popupInfo} = this.state;
        return (
            popupInfo && (
                <Popup
                    className="avocado"
                    tipSize={5}
                    anchor="bottom"
                    longitude={ADDRESS.longitude}
                    latitude={ADDRESS.latitude}
                    closeOnClick={true}
                    onClose={() => this.setState({popupInfo: null})}
                >
                    <p><strong>Vorstadt 27 <br/> 97225 Zellingen</strong></p>
                </Popup>
            )
        );
    }

    render() {

        const {viewport} = this.state;

        return <div>
            <MapGL
                {...viewport}
                width="100%"
                height="50vh"
                mapStyle="mapbox://styles/d4n13l/ck7fxd60t3o3g1irw6grnsr62"
                onViewportChange={this.onChangeViewport}
                mapboxApiAccessToken={API_TOKEN}
            >
                <MapMarker
                    address={ADDRESS}
                    onClick={this._onClickMarker}
                />
                {this.__renderPopup()}
                <div style={fullscreenControlStyle}>
                    <FullscreenControl/>
                </div>
                <div style={navStyle}>
                    <NavigationControl/>
                </div>
                <div style={scaleControlStyle}>
                    <ScaleControl/>
                </div>

            </MapGL>
        </div>


    }


}

export default Map;