import React, {Component} from 'react';
import {NavHashLink as NavLink} from 'react-router-hash-link';
import {MDBIcon} from "mdbreact";

class Footer extends Component {

    render() {
        return <footer className="page-footer font-small dark-ink">

            <div className="container-fluid text-center pt-2">


                <div className="row justify-content-center text-center">

                    <div className="col-sm-3 mb-2">

                        <a className="link-footer"
                           href="mailto:info@weidenflechten-zellingen.de?subject=Anfrage">Kontakt:
                            E-Mail senden</a>
                    </div>


                    <div className="col-sm-3 mb-2">

                        <NavLink className="link-footer" to="/impressum&dsgvo/#impressum">Impressum</NavLink>
                    </div>


                    <div className="col-sm-3 mb-2">

                        <NavLink className="link-footer" to="/impressum&dsgvo/#dsgvo">Datenschutz</NavLink>

                    </div>
                    <div className="col-sm-3 mb-2">
                        <a href="https://www.pinterest.de/volkerwingenfel/" className="mx-3 align-middle">
                            <MDBIcon fab icon="pinterest" size={"2x"}/>
                        </a>
                        <a className="mx-3 align-middle" href="https://www.instagram.com/weidenflechten.zellingen/">
                            <MDBIcon fab icon="instagram" size={"2x"}/>
                        </a>
                    </div>
                </div>
            </div>

            <div className="footer-copyright text-center pt-3">
                <p className="copyright">© 2020 Copyright: weidenfelchten-zellingen.de</p>
            </div>

        </footer>
    }
}

export default Footer;