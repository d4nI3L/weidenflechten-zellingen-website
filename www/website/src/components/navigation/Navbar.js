import React, {Component} from 'react';
import 'bootstrap-css-only/css/bootstrap.min.css';
import '../../index.css';
import FullSizeBanner from "../banner/FullSizeBanner";
import {NavHashLink as NavLink} from 'react-router-hash-link';

import {
    MDBCollapse,
    MDBContainer,
    MDBHamburgerToggler,
    MDBNavbar,
    MDBNavbarBrand,
    MDBNavbarNav,
    MDBNavItem, MDBNavLink,
} from "mdbreact";
import GrowMenu from "./GrowMenu";


class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isWideEnough: false,
            collapseID: ''
        };

        this.toggleCollapse = this.toggleCollapse.bind(this);

    }

    toggleCollapse = collapseID => () => {
        this.setState(prevState => ({
            collapseID: prevState.collapseID !== collapseID ? collapseID : ""
        }));
    };


    render() {
        return (
            <div>
                <header>
                    <MDBNavbar color="dark-ink" fixed="top" dark expand="lg">
                        <MDBContainer>
                            <MDBNavbarBrand className="navbar-brand float-left pl-3">
                                <MDBNavLink to="/"><img loading="lazy" height="50" src={"https://www.weidenflechten-zellingen.de/img/logo.png"} alt="Weidenflechten-Zellingen"/></MDBNavLink>
                            </MDBNavbarBrand>
                            <MDBHamburgerToggler color="#d4d4d4" id="hamburger1" className={"navbar-toggler"}
                                                 onClick={this.toggleCollapse("basicCollapse")}/>
                            <MDBCollapse id="basicCollapse"
                                         className={"collapse navbar-collapse justify-content-lg-center"}
                                         isOpen={this.state.collapseID} navbar>
                                <MDBNavbarNav centered="true">
                                    <MDBNavItem>
                                        <MDBNavLink to="/">Home</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <NavLink className="nav-link" to="/#about">
                                            Über Uns
                                        </NavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <NavLink className="nav-link" to="/#kurse">Kurse & Kontakt</NavLink>
                                    </MDBNavItem>
                                    <GrowMenu/>
                                </MDBNavbarNav>
                            </MDBCollapse>
                        </MDBContainer>
                    </MDBNavbar>

                    <FullSizeBanner imgName="banner.jpeg"/>

                </header>
            </div>
        );
    }
}

export default Navbar;