import React, {Component} from 'react';
import Navbar from '../components/navigation/Navbar';
import '../index.css';
import Footer from "../components/footer/Footer";
import Cards from "../components/landing/Cards";
import {MDBContainer} from "mdbreact";
import CoursesAndContact from "../components/landing/CoursesAndContact";
import AboutUs from "../components/landing/AboutUs";
import {CardContentLanding} from "../constants/Contents";
import Craft from "../components/landing/Craft";

class Home extends Component {

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return <div>
            <Navbar/>
                <main>
                    <MDBContainer className="text-center my-5">
                        <p className="huge-text">In einer Auswahl von Bildern zu unseren Weidenkörben, Korbtaschen und
                            Weidendeko zeigen wir gerne unsere Flechtarbeiten
                        </p>
                    </MDBContainer>
                </main>
                <Cards content={CardContentLanding}/>
                <AboutUs/>
                <Craft/>
                <CoursesAndContact/>
            <Footer/>
        </div>
    }
}

export default Home;