import React, {Component} from 'react';
import Navbar from "../components/navigation/Navbar";
import Footer from "../components/footer/Footer";
import {Element} from 'react-scroll';

class ImprintDataProtection extends Component {

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return <div>
            <Navbar/>
            <div className="spacer"/>
            <Element id="impressum" name="impressum">
            <div className="card card-cascade wider reverse text-container">
                <div className="card-body card-body-cascade">
                    <div className="text-center">
                        <h2 className="card-title content-text-dark"><strong>Angaben gemäß §5 TMG:</strong></h2>
                        <br/>
                        <h3 className="font-weight-bold py-2 content-text-dark">Weidenflechten Zellingen</h3>
                        <br/>
                    </div>
                    <div className="tex-center row">
                        <div className="col-lg-3 emphasised-text">
                            <h4 className="font-weight-bold content-text-dark">Vertreten durch</h4>
                            <br/>
                            <p className="my-card-text">Sabine Dluczek</p>
                            <p className="my-card-text">Vorstadt 27</p>
                            <p className="my-card-text">97225 Zellingen</p>
                            <br/>
                        </div>
                        <div className="col-lg-6 emphasised-text">
                            <h4 className="font-weight-bold content-text-dark">Kontakt:</h4>
                            <br/>
                            <p className="my-card-text">Telefon: 09364 4671</p>
                            <p className="my-card-text">E-Mail:
                                <a className="avocado"
                                   href="mailto:info@weidenflechten-zellingen.de?subject=Anfrage"> info@weidenflechten-zellingen.de</a>
                            </p>
                            <br/>
                        </div>
                        <div className="col-lg-12">
                            <h3 className="font-weight-bold content-text-dark">Urheberrecht</h3>
                            <div className="my-card-text continuous-text">
                                Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen
                                Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung,
                                Verbreitung und
                                jede Art
                                der
                                Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen
                                Zustimmung des
                                jeweiligen
                                Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten,
                                nicht
                                kommerziellen
                                Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt
                                wurden, werden
                                die
                                Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche
                                gekennzeichnet.
                                Sollten Sie
                                trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen
                                entsprechenden
                                Hinweis. Bei
                                Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend
                                entfernen.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </Element>
            <div className="spacer-small"/>
            <Element id="dsgvo" name="dsgvo">
                <div className="card card-cascade wider reverse text-container">
                    <div className="card-body card-body-cascade">
                        <div className="text-center">
                            <h2 className="card-title content-text-dark"><strong>Datenschutzerkärung</strong></h2>
                            <br/>
                        </div>
                        <div className="tex-center">
                            <div className="my-3 continuous-text my-card-text">
                                <div className="continuous-text">
                                    Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten
                                    möglich. Soweit
                                    auf
                                    unseren
                                    Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen)
                                    erhoben werden,
                                    erfolgt
                                    dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre
                                    ausdrückliche
                                    Zustimmung
                                    nicht an Dritte weitergegeben. <br/>
                                    <br/>
                                </div>
                                <div className="continuous-text">
                                    Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der
                                    Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz
                                    der
                                    Daten vor dem
                                    Zugriff
                                    durch Dritte ist nicht möglich. <br/>
                                    <br/>
                                </div>
                                <div className="continuous-text">
                                    Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten
                                    durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und
                                    Informationsmaterialien wird
                                    hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich
                                    ausdrücklich
                                    rechtliche
                                    Schritte
                                    im
                                    Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails,
                                    vor. <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Element>
            <div className="spacer"/>
            <Footer/>
        </div>
    }
}

export default ImprintDataProtection;