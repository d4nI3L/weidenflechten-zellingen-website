import React from 'react';
import {MDBMask, MDBView} from "mdbreact";
import {Link} from "react-router-dom";

const NotFound = () => <MDBView src={"https://www.weidenflechten-zellingen.de/img/burningBasket.jpg"}>
    <MDBMask overlay="black-light" className="flex-center flex-column text-white text-center">
        <div className="mb-2">
            <p className="nice-white-text bigger-text white-text"> Oooops hier gibt wohl es keine Weidenkörbe mehr!</p>
        </div>
        <div className="mt-5">
            <Link to="/" className=" nice-white-text strong-link">
                Hier geht es zu unserer Startseite
            </Link>
        </div>
    </MDBMask>
</MDBView>


export default NotFound;