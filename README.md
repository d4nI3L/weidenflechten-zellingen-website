# This is the website for Weidenflechten-Zellingen.de

The goal of this project is to modernize the website "weidenflechten-zellingen.de" and to create a fully mobile responsive website. 
Furthermore, this is my personal project to practice my frontend skills a bit because I am normally focused on backend and data science. 
React might be an overkill for a private website with only a view pages, but it keeps me on track with the technology my frontend dev colleagues use at work. 

## Future work
* Switch hosting service to cloud (AWS or AZURE) to have more control
* Extend Content and pages after cloud migration 
* Optimize crawlability for bots (SEO)
* Optimize loading time
